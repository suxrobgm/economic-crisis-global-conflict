﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EC.Scripts.Common
{
    public class Country : MonoBehaviour
    {
        public string tagName;
        public string longName;
        public string shortName;
        public ulong population;
        public Color colorInMap;
        public City capital;
        public Leader leader;
        public List<Party> parties;
        public Party rullingParty;
        public List<State> states;
        public AudioSource nationalAnthem;
        public CountryEconomics economics;
    }
}