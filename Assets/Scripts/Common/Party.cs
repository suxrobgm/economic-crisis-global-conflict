﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EC.Scripts.Common
{
    public class Party : MonoBehaviour
    {
        public string partyName;
        public double popularity;
    }
}