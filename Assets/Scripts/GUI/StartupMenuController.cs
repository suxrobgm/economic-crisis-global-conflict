﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace EC.Scripts.GUI
{
    public class StartupMenuController : MonoBehaviour
    {
        public Button openGame;
        public Button quit;
        public Button settings;

        private void Start()
        {
            //var btn = GameObject.Find("Open Game Button").GetComponent<Button>();
            //btn.onClick.AddListener(OpenGameClick);
            openGame.onClick.AddListener(OpenGameClick);
        }

        public void OpenGameClick()
        {
            SceneManager.LoadScene("MainMap");
        }
    }
}