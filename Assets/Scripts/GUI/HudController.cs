﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EC.Scripts.GUI
{
    public class HudController : MonoBehaviour
    {
        private DateTime _gameTime;
        private GameStats _gameStats;

        public Text gameTime;

        private void Start()
        {
            _gameTime = DateTime.Now;
            gameTime.text = _gameTime.ToString("hh:00, dd MMM, yyyy");
            _gameStats = GameStats.instance;
        }

        private void Update()
        {
            if (!_gameStats.gamePaused)
            {
                _gameTime = _gameTime.AddHours(1.0);
                gameTime.text = _gameTime.ToString("hh:00, dd MMM, yyyy");
            }

            if (Input.GetKeyDown(KeyCode.Space) && _gameStats.gamePaused)
            {
                _gameStats.gamePaused = false;
            }
            else if (Input.GetKeyDown(KeyCode.Space) && !_gameStats.gamePaused)
            {
                _gameStats.gamePaused = true;
            }
        }
    }
}