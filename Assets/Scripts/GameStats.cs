﻿using System;

namespace EC.Scripts
{
    public class GameStats
    {
        private static GameStats _instance;

        private GameStats()
        {
            gamePaused = false;
        }

        public static GameStats instance
        {
            get
            {
                if (_instance == null)                
                    _instance = new GameStats();
                
                return _instance;
            }
        }
        public bool gamePaused { get; set; }       
    }
}
