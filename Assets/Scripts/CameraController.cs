﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EC.Scripts
{
    public class CameraController : MonoBehaviour
    {
        private Camera _camera;

        private void Awake()
        {
            _camera = GetComponent<Camera>();
        }

        private void Update()
        {
            Zoom();
            Move();
        }

        private void Move()
        {
            var currentPos = _camera.transform.position;

            if (Input.GetKey(KeyCode.RightArrow))
                _camera.transform.position = new Vector3(++currentPos.x, currentPos.y, currentPos.z);

            if (Input.GetKey(KeyCode.LeftArrow))          
                _camera.transform.position = new Vector3(--currentPos.x, currentPos.y, currentPos.z);

            if (Input.GetKey(KeyCode.UpArrow))            
                _camera.transform.position = new Vector3(currentPos.x, currentPos.y, ++currentPos.z);            

            if (Input.GetKey(KeyCode.DownArrow))
                _camera.transform.position = new Vector3(currentPos.x, currentPos.y, --currentPos.z);
            
        }

        private void Zoom()
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0f) // forward
            {
                _camera.orthographicSize++;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") > 0f) // backwards
            {
                _camera.orthographicSize--;
            }
        }
    }
}